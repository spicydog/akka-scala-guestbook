FROM mozilla/sbt as build
COPY . .
RUN [ "sbt", "universal:packageBin" ]
RUN [ "unzip", "/target/universal/akka-http-kubernetes-0.1.0-SNAPSHOT.zip" ]
RUN [ "mv", "akka-http-kubernetes-0.1.0-SNAPSHOT", "app" ]
RUN [ "cp", "-r", "/res", "/app/bin/" ]

FROM openjdk:8-jre-alpine as mainstage
RUN apk add --no-cache bash
USER root
RUN id -u demiourgos728 1>/dev/null 2>&1 || (( getent group 0 1>/dev/null 2>&1 || ( type groupadd 1>/dev/null 2>&1 && groupadd -g 0 root || addgroup -g 0 -S root )) && ( type useradd 1>/dev/null 2>&1 && useradd --system --create-home --uid 1001 --gid 0 demiourgos728 || adduser -S -u 1001 -G root demiourgos728 ))
COPY --from=build --chown=demiourgos728:root /app /app
USER 1001:0
EXPOSE 8080
WORKDIR /app/bin
ENTRYPOINT ["/app/bin/akka-http-kubernetes"]
CMD []
