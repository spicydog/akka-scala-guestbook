package com.example

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._

import scala.io.Source

class HomeRoute {

  def route: Route =
    (get & path("")) {
      pathEndOrSingleSlash {
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, {
          val indexFilePath = "res/index.html"
          Source.fromFile(indexFilePath).mkString
        }))
      }
    }
}
