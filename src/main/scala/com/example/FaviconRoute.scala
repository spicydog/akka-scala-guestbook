package com.example

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._

import scala.io.Source

class FaviconRoute {

  def route: Route =
    (get & path("favicon.ico")) {
      pathEndOrSingleSlash {
        redirect("https://cdn.spicydog.org/favicon.ico", StatusCodes.PermanentRedirect)
      }
    }
}
