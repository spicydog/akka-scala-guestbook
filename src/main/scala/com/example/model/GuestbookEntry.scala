package com.example.model

import io.circe.{ Decoder, Encoder }
import io.circe.generic.semiauto.{ deriveDecoder, deriveEncoder }
import java.time.LocalDateTime

case class GuestbookEntry(val author: String, val content: String) {
  val createdAt = LocalDateTime.now()
}

object GuestbookEntry {
  implicit val decoder: Decoder[GuestbookEntry] = deriveDecoder
  implicit val encoder: Encoder[GuestbookEntry] =
  Encoder.forProduct3("author", "content", "created_at") (entry =>
    (
      entry.author, 
      entry.content, 
      entry.createdAt
    )
  )

}
