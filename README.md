# Akka Scala Guestbook

I project for my Scala and Akka learning based on an example of Akka Guestbook project.

What I did is adding more information to the message, UI, and DevOps to deploy on Google Cloud Run.

Here is the demo: https://akka-scala-guestbook-rb3x7zx6ha-uc.a.run.app/